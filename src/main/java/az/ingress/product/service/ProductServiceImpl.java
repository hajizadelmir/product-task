package az.ingress.product.service;

import az.ingress.product.dto.ProductDto;
import az.ingress.product.model.Product;
import az.ingress.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final ModelMapper mapper;

    @Override
    public ProductDto getProductById(Long id) {
        Product byId = repository.getById(id);
        ProductDto map = mapper.map(byId, ProductDto.class);
        return map;
    }

    @Override
    public ProductDto createProduct(ProductDto dto) {
        Product product = mapper.map(dto, Product.class);
        Product save = repository.save(product);
        return mapper.map(save, ProductDto.class);
    }

    @Override
    public void deleteProduct(Long id) {
        repository.deleteById(id);

    }

    @Override
    public ProductDto updateProduct(ProductDto dto) {
        Product product = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Product not Found!")));
        product.setFirstName(dto.getFirstName());
        product.setPrice(dto.getPrice());
        product.setCreateDate(dto.getCreateDate());
        product.setBrand(dto.getBrand());
        product.setCount(dto.getCount());
        product.setIsActive(dto.getIsActive());
        Product save = repository.save(product);
        ProductDto productDto = mapper.map(save, ProductDto.class);
        return productDto;
    }

    @Override
    public List<ProductDto> findAllProduct() {

        List<Product> all = repository.findAll();
        List<ProductDto> e = all.stream().map(a -> mapper.map(a, ProductDto.class)).collect(Collectors.toList());
        return e;
    }
}
