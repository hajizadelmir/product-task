package az.ingress.product.service;

import az.ingress.product.dto.ProductDto;

import java.util.List;

public interface ProductService {
    public ProductDto getProductById(Long id);

   public ProductDto createProduct(ProductDto dto);

   public void deleteProduct(Long id);

    public ProductDto updateProduct(ProductDto dto);

    List<ProductDto> findAllProduct();

}
