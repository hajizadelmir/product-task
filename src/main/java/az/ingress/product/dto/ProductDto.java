package az.ingress.product.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProductDto {
    private Long id;
    private String firstName;
    private Double price;
    private LocalDate createDate;
    private String  brand;
    private Integer count;
    private Boolean isActive;

}
